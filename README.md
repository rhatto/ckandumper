# CKAN Dumper

Downloads both metadata and datasets from a CKAN instance.

## Dependencies

Works on python 3.7 with the following dependencies met:

    apt install wget python3-tqdm

## Usage

    ckandumper --help
